﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Abstract Messages for Caller" Type="Folder">
		<Item Name="Calc Results Msg.lvclass" Type="LVClass" URL="../Sample PPL Actor Messages/Calc Results Msg/Calc Results Msg.lvclass"/>
		<Item Name="Step Done Msg.lvclass" Type="LVClass" URL="../Sample PPL Actor Messages/Step Done Msg/Step Done Msg.lvclass"/>
	</Item>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="Echo Msg.lvclass" Type="LVClass" URL="../Sample PPL Actor Messages/Echo Msg/Echo Msg.lvclass"/>
		<Item Name="Long Process Done Msg.lvclass" Type="LVClass" URL="../Sample PPL Actor Messages/Long Process Done Msg/Long Process Done Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Long Process Msg.lvclass" Type="LVClass" URL="../Sample PPL Actor Messages/Long Process Msg/Long Process Msg.lvclass"/>
		<Item Name="Perform Calc Msg.lvclass" Type="LVClass" URL="../Sample PPL Actor Messages/Perform Calc Msg/Perform Calc Msg.lvclass"/>
		<Item Name="Send Boolean Msg.lvclass" Type="LVClass" URL="../Sample PPL Actor Messages/Send Boolean Msg/Send Boolean Msg.lvclass"/>
	</Item>
	<Item Name="Sample Actor.lvclass" Type="LVClass" URL="../Sample PPL Actor/Sample Actor.lvclass"/>
</Library>
